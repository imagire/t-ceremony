# t-ceremony: A Continuous Publishing system #

### What is this repository for? ###

* This project comes true a continuous publishing in free using Bitbucket and Re:VIEW and wercker. When Re:VIEW files are committed in a Bitbucket repository, CI makes a complied pdf and ships to another Bitbucket repository.

### How do I get set up? ###

* download this repo's files.
* write put [Re:VIEW sample](https://github.com/takahashim/review-sample-book) or write Re:VIEW files in the src directory.
* push to your Bitbucket repository.
* create another Bitbucket repository for deploy.
* add app in wercker in terms for your 1st repository.
* set variables for your app.
    * username: your Bitbucket account
    * password: Bitbucket's password
    * email: e-mail address for committing on git 
    * teamname: if 2nd repository is created on teams in Bitbucket, enter team name otherwise your Bitbucket account name
    * deploy_repository: 2nd repository's name
* set deploy target to 2nd repository

See details at http://www.slideshare.net/imagire/tceremony (in japanease)

### Contribution guidelines ###

* Writing tests: please write if possible.

* Code review: we will review your pull requests.

### Who do I talk to? ###

* Repo owner: Takashi Imagire

### Rreference ###

* @takahashim: [[ReVIEW Tips] DockerでRe:VIEW](http://qiita.com/takahashim/items/406421d515ef1d4f1189)